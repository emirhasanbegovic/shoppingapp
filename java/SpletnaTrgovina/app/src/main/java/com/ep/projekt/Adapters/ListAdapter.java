package com.ep.projekt.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.ep.projekt.Models.IzdelekKosarica;
import com.ep.projekt.R;

import java.util.ArrayList;

public class ListAdapter extends ArrayAdapter<IzdelekKosarica> {

    int vg;
    ArrayList<IzdelekKosarica>kosaricaArrayList;
    Context context;

    public ListAdapter(Context context, int vg, int id, ArrayList<IzdelekKosarica> kosaricaArrayList){
        super(context, vg, id, kosaricaArrayList);
        this.context = context;
        this.kosaricaArrayList = kosaricaArrayList;
        this.vg = vg;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View rowView = convertView;
        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(vg, parent, false);
        }

        Button buttonRemoveItem = rowView.findViewById(R.id.btnremove);

        buttonRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                remove(getItem(position));
            }
        });

        IzdelekKosarica izdelekKosarica = getItem(position);

        TextView textViewModel = rowView.findViewById(R.id.txtmodel);
        TextView textViewQuantity = rowView.findViewById(R.id.txtamount);
        TextView textViewPrice = rowView.findViewById(R.id.txtprice);


        textViewModel.setText(izdelekKosarica.getImeIzdelka());
        textViewQuantity.setText(""+izdelekKosarica.getKolicina());
        textViewPrice.setText(izdelekKosarica.getCenaIzdelka());


        return rowView;
    }
}
