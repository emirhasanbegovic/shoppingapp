package com.ep.projekt.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ep.projekt.Adapters.ListAdapter;
import com.ep.projekt.Models.IzdelekKosarica;
import com.ep.projekt.R;
import com.ep.projekt.Singelton.Singelton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;


public class KosaricaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kosarica_layout);
        final Singelton singelton = Singelton.getInstance();

        ListView listView = findViewById(R.id.listv);
        RelativeLayout footer = (RelativeLayout) getLayoutInflater().inflate(R.layout.footer, null);
        listView.addFooterView(footer);

        ViewGroup headerView = (ViewGroup)getLayoutInflater().inflate(R.layout.header, listView, false);
        listView.addHeaderView(headerView);


        final ListAdapter adapter = new ListAdapter(this, R.layout.rowlayout, R.id.txtmodel,singelton.getIzdelekKosaricaArrayList());
        listView.setAdapter(adapter);

        Button button = headerView.findViewById(R.id.btnremoveAll);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.clear();
                adapter.notifyDataSetChanged();
            }
        });

        Button confirmButton = listView.findViewById(R.id.cart_confirmation);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<IzdelekKosarica> izdelekKosaricas = singelton.getIzdelekKosaricaArrayList();
                if(singelton.getIzdelekKosaricaArrayList().isEmpty()){
                    Toast.makeText(KosaricaActivity.this, "Empty cart", Toast.LENGTH_LONG).show();
                    return;
                }
                JSONObject jsonObjectSEND = new JSONObject();

                final double skupnaCena = singelton.skupnaCena();

                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH) + 1;
                int date = c.get(Calendar.DATE);

                String monthString = "";
                String dateString = "";
                if(month < 10){
                    monthString = "0" + month;
                }else{
                    monthString = "" + month;
                }

                if(date < 10){
                    dateString = "0" + date;
                }else{
                    dateString = "" + date;
                }

                String datum_oddano = dateString + "/" + monthString + "/" + year;


                JSONArray jsonArray = new JSONArray();

                for(int i = 0; i < izdelekKosaricas.size(); i++){
                    IzdelekKosarica izdelekKosarica = izdelekKosaricas.get(i);
                    int idIzdelka = izdelekKosarica.getIdIzdelka();
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("Artikel_id", idIzdelka);
                        jsonObject.put("quantity", izdelekKosarica.getKolicina());
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    jsonObjectSEND.put("datum_oddano", datum_oddano);
                    jsonObjectSEND.put("skupnaCena", skupnaCena);
                    jsonObjectSEND.put("Stranka_id", singelton.getUserID());
                    jsonObjectSEND.put("izdelki", jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("jsonSEND", jsonObjectSEND.toString());


                JsonObjectRequest jsonObjectRequestect = new JsonObjectRequest(Request.Method.POST, singelton.getUrl()+"narocilo", jsonObjectSEND, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String msg = response.getString("message");

                            if(msg.equals("Successfull.")){
                                adapter.clear();
                                adapter.notifyDataSetChanged();
                                Toast.makeText(KosaricaActivity.this, "Successfull", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(KosaricaActivity.this, "Paymet failed", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(KosaricaActivity.this, "Paymet failed", Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        final Map<String, String> headers = new HashMap<>();
                        return headers;
                    }
                };

                singelton.getQueue(KosaricaActivity.this).add(jsonObjectRequestect);


            }
        });

    }

}
