package com.ep.projekt.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.ep.projekt.R;
import com.ep.projekt.Singelton.Singelton;

public class SlideAdapters extends PagerAdapter {

    Context context;
    LayoutInflater inflater;
    private Singelton singelton;

    public SlideAdapters(Context context) {
        this.context = context;
        singelton = Singelton.getInstance();
    }


    @Override
    public int getCount() {
        Log.i("getcount", ""+singelton.getStIzdelkov());
        return singelton.getStIzdelkov();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view == o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide, container, false);
        Log.i("GAAAAAAAAAAAAAA", "myview"+position);
        view.setTag("myview"+position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }


}