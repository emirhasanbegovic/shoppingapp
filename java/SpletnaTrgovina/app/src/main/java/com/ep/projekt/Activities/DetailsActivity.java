package com.ep.projekt.Activities;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.ep.projekt.Models.IzdelekKosarica;
import com.ep.projekt.Models.Product;
import com.ep.projekt.R;
import com.ep.projekt.Singelton.Singelton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailsActivity extends AppCompatActivity {

    private Singelton singelton;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        singelton = Singelton.getInstance();

        int key = getIntent().getIntExtra("position", -1);
        final Product product = singelton.getHmProducts().get(key);
        getMoreInformation(key);


        Button btn = findViewById(R.id.dodajVKosarico);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DetailsActivity.this);
                builder.setTitle("Quantity");

                final EditText input = new EditText(DetailsActivity.this);
                input.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String userInput = input.getText().toString();
                        try{

                            int kolicina = Integer.parseInt(userInput);

                            int indexIzdelka = singelton.findProductWithGivenID(product.getProductID());
                            Log.i("idIzdelka = ", ""+product.getProductID());
                            if(indexIzdelka == -1){
                                singelton.getIzdelekKosaricaArrayList().add(new IzdelekKosarica(product.getNaziv(), kolicina, product.getProductID(), ""+product.getCena()+" €"));
                            }else{
                                singelton.setKolicina(product.getProductID(),kolicina);
                            }



                        }catch (Exception e){
                            Log.i("i accept numbers only", e.toString());
                            e.printStackTrace();
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });



    }

    public void getMoreInformation(int key) {
        final Product product = singelton.getHmProducts().get(key);
        setStars(product);
        setTextViews(product);

        if (product.getSlike() != null) {
            appendPictures(product);
            return;
        }


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, singelton.getUrl() + "slika-artikel/" + product.getProductID(), null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("TAG", response.toString());
                        Bitmap bitmap[] = new Bitmap[response.length()];
                        for (int i = 0; i < bitmap.length; i++) {
                            Log.i("HADUKEN", ""+bitmap.length);
                            try {
                                String blobData = ((JSONObject)response.get(i)).getString("slika");
                                Log.i("HADUKEN", ""+blobData);
                                byte decodedString[] = Base64.decode(blobData.split(",")[1], Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                bitmap[i] = decodedByte;

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("HADUKEN", ""+bitmap.length);
                        product.setSlike(bitmap);
                        appendPictures(product);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HADUKEN NO", error.toString());

                    }
                });

        singelton.getQueue(DetailsActivity.this).add(jsonArrayRequest);

    }

    private void appendPictures(Product product) {
        LinearLayout gridLayout = findViewById(R.id.linearLayoutID);
        Bitmap arr[] = product.getSlike();
        for(int i = 0; i < arr.length; i++){
            ImageView imageView = new ImageView(DetailsActivity.this);
            imageView.setPadding(20,20,20,20);
            imageView.setImageBitmap(arr[i]);
            imageView.setAdjustViewBounds(true);
            gridLayout.addView(imageView);
        }
    }

    private void setTextViews(Product product){
        TextView textViewNaziv = findViewById(R.id.nazivArtikla);
        TextView textViewFirma = findViewById(R.id.firmaArtikla);
        TextView textViewCena = findViewById(R.id.cenaArtikla);
        TextView textViewOpis = findViewById(R.id.opisArtikla);
        TextView textViewKategorija= findViewById(R.id.kategorijaArtikla);

        textViewCena.setMovementMethod(new ScrollingMovementMethod());
        textViewFirma.setMovementMethod(new ScrollingMovementMethod());
        textViewCena.setMovementMethod(new ScrollingMovementMethod());
        textViewOpis.setMovementMethod(new ScrollingMovementMethod());
        textViewKategorija.setMovementMethod(new ScrollingMovementMethod());

        textViewNaziv.setText(product.getNaziv());
        textViewFirma.setText(product.getFirma());
        textViewCena.setText(product.getCena() + " €");
        textViewOpis.setText(product.getOpis());
        textViewKategorija.setText(product.getKategorija());

    }

    private void setStars(Product product){
       RatingBar ratingBar = findViewById(R.id.ratingBar2);
       ratingBar.setRating(product.getOcena());
    }


}
