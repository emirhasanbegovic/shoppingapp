package com.ep.projekt.Models;

public class IzdelekKosarica {

    private String imeIzdelka;
    private int kolicina;
    private int idIzdelka;
    private String cenaIzdelka;

    public IzdelekKosarica(String imeIzdelka, int kolicina, int idIzdelka, String cenaIzdelka) {
        this.imeIzdelka = imeIzdelka;
        this.kolicina = kolicina;
        this.idIzdelka = idIzdelka;
        this.cenaIzdelka = cenaIzdelka;
    }

    @Override
    public String toString() {
        return "IzdelekKosarica{" +
                "imeIzdelka='" + imeIzdelka + '\'' +
                ", kolicina=" + kolicina +
                ", idIzdelka=" + idIzdelka +
                ", cenaIzdelka='" + cenaIzdelka + '\'' +
                '}';
    }

    public String getImeIzdelka() {
        return imeIzdelka;
    }

    public void setImeIzdelka(String imeIzdelka) {
        this.imeIzdelka = imeIzdelka;
    }

    public int getKolicina() {
        return this.kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public int getIdIzdelka() {
        return idIzdelka;
    }

    public void setIdIzdelka(int idIzdelka) {
        this.idIzdelka = idIzdelka;
    }

    public String getCenaIzdelka() {
        return cenaIzdelka;
    }

    public void setCenaIzdelka(String cenaIzdelka) {
        this.cenaIzdelka = cenaIzdelka;
    }
}
