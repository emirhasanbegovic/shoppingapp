package com.ep.projekt.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ep.projekt.R;
import com.ep.projekt.Singelton.Singelton;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ActivitySettings extends AppCompatActivity {

    private Singelton singelton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        singelton = Singelton.getInstance();

        EditText editTextIme = findViewById(R.id.txtime);
        EditText editTextPriimek = findViewById(R.id.txtpriimek);
        EditText editTextNaslov = findViewById(R.id.txtnaslov);
        EditText editTextTelefon = findViewById(R.id.txttelefon);

        editTextIme.setText(singelton.getIme());
        editTextPriimek.setText(singelton.getPriimek());
        editTextNaslov.setText(singelton.getNaslov());
        editTextTelefon.setText(singelton.getTelefon());

        Button confirmButton = findViewById(R.id.button_confirm_changes);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserData();
            }
        });

    }

    public void updateUserData(){

        EditText editTextIme = findViewById(R.id.txtime);
        EditText editTextPriimek = findViewById(R.id.txtpriimek);
        EditText editTextNaslov = findViewById(R.id.txtnaslov);
        EditText editTextTelefon = findViewById(R.id.txttelefon);

        String ime = editTextIme.getText().toString();
        String priimek = editTextPriimek.getText().toString();
        String naslov = editTextNaslov.getText().toString();
        String telefon = editTextTelefon.getText().toString();


        if(!check(ime) || !check(priimek)  || !check(naslov)  || !telefon.matches("^\\d{3}-\\d{3}-\\d{3}$")){
            Toast.makeText(ActivitySettings.this, "Please fill the input form", Toast.LENGTH_LONG).show();
            return;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ime", ime);
            jsonObject.put("priimek", priimek);
            jsonObject.put("naslov", naslov);
            jsonObject.put("telefon", telefon);

            JsonObjectRequest jsonObjectRequestect = new JsonObjectRequest(Request.Method.PUT, singelton.getUrl()+"stranka/account/"+singelton.getUserID(), jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String msg = response.getString("message");
                        if(msg.equals("Successfull.")){
                            Toast.makeText(ActivitySettings.this, "Successfull", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(ActivitySettings.this, "Please fill the input form", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ActivitySettings.this, "Please fill the input form", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    return headers;
                }
            };

            singelton.getQueue(ActivitySettings.this).add(jsonObjectRequestect);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean check(String string){
        return string.length()>3 && string.length() < 255;
    }
}
