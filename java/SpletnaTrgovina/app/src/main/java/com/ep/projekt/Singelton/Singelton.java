package com.ep.projekt.Singelton;

import android.content.Context;
import android.util.Log;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ep.projekt.Models.IzdelekKosarica;
import com.ep.projekt.Models.Product;

import java.util.ArrayList;
import java.util.HashMap;

public class Singelton {

    private static Singelton instance = null;
    private static int userID;
    private static String ime;
    private static String priimek;
    private static String naslov;
    private static String telefon;

    private int stIzdelkov;

    private static final String URL = "http://192.168.64.107/netbeans/REST-API/api/";

    private ArrayList<IzdelekKosarica> izdelekKosaricaArrayList = new ArrayList<IzdelekKosarica>();
    private HashMap<Integer, Product> hmProducts = new HashMap<Integer, Product>();
    private static RequestQueue queue;

    protected Singelton(){

    }

    public static Singelton getInstance(){
        if(instance == null){
            instance =  new Singelton();
        }
        return instance;
    }


    public ArrayList<IzdelekKosarica> getIzdelekKosaricaArrayList() {
        return izdelekKosaricaArrayList;
    }

    public void setIzdelekKosaricaArrayList(ArrayList<IzdelekKosarica> izdelekKosaricaArrayList) {
        this.izdelekKosaricaArrayList = izdelekKosaricaArrayList;
    }

    public int findProductWithGivenID(int id){
        for(int i = 0; i < izdelekKosaricaArrayList.size(); i++){
            if(izdelekKosaricaArrayList.get(i).getIdIzdelka() == id){
                return i;
            }
        }
        return -1;
    }

    public void setKolicina(int id, int kolicina){
        for(int i = 0; i < izdelekKosaricaArrayList.size(); i++){
            if(izdelekKosaricaArrayList.get(i).getIdIzdelka() == id){
                izdelekKosaricaArrayList.get(i).setKolicina(kolicina);
                return;
            }
        }
    }

    public void out(){
        for(IzdelekKosarica izdelekKosarica : izdelekKosaricaArrayList){
            Log.i("out: ", ""+izdelekKosarica.toString() + "\n");
        }
    }

    public String getUrl(){
        return URL;
    }

    public int getUserID(){
        return userID;
    }

    public void setUserID(int userID){
        this.userID = userID;
    }

    public HashMap<Integer, Product> getHmProducts() {
        return hmProducts;
    }

    public void setHmProducts(HashMap<Integer, Product> hmProducts) {
        this.hmProducts = hmProducts;
    }

    public RequestQueue getQueue(Context context){
        return Volley.newRequestQueue(context);
    }

    public double skupnaCena(){
        double sum = 0;
        for(int i = 0; i < izdelekKosaricaArrayList.size(); i++){
            IzdelekKosarica izdelekKosarica = izdelekKosaricaArrayList.get(i);
            String cena = izdelekKosarica.getCenaIzdelka();
            double cenaDouble = Double.parseDouble(cena.split(" ")[0]) * izdelekKosarica.getKolicina();
            sum+=cenaDouble;
        }

        return sum;
    }

    public static String getIme() {
        return ime;
    }

    public static void setIme(String ime) {
        Singelton.ime = ime;
    }

    public static String getPriimek() {
        return priimek;
    }

    public static void setPriimek(String priimek) {
        Singelton.priimek = priimek;
    }

    public static String getNaslov() {
        return naslov;
    }

    public static void setNaslov(String naslov) {
        Singelton.naslov = naslov;
    }

    public static String getTelefon() {
        return telefon;
    }

    public static void setTelefon(String telefon) {
        Singelton.telefon = telefon;
    }


    public int getStIzdelkov() {
        return stIzdelkov;
    }

    public void setStIzdelkov(int stIzdelkov) {
        this.stIzdelkov = stIzdelkov;
    }

}
