package com.ep.projekt.Models;

import android.graphics.Bitmap;

public class Product {

    private int productID;
    private String naziv;
    private double cena;
    private String opis;
    private String kategorija;
    private String statusIzdelka;
    private Bitmap slika1;
    private Bitmap slike [];
    private String firma;
    private int ocena;

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }



    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getStatusIzdelka() {
        return statusIzdelka;
    }

    public void setStatusIzdelka(String statusIzdelka) {
        this.statusIzdelka = statusIzdelka;
    }

    public Bitmap getSlika1() {
        return slika1;
    }

    public void setSlika1(Bitmap slika1) {
        this.slika1 = slika1;
    }

    public Bitmap[] getSlike() {
        return slike;
    }

    public void setSlike(Bitmap[] slike) {
        this.slike = slike;
    }



}
