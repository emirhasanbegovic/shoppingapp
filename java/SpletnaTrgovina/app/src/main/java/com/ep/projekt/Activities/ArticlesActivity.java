package com.ep.projekt.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ep.projekt.Adapters.SlideAdapters;
import com.ep.projekt.Models.Product;
import com.ep.projekt.R;
import com.ep.projekt.Singelton.Singelton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ArticlesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    private SlideAdapters slideAdapter;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private Singelton singelton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);
        singelton = Singelton.getInstance();

        mDrawerLayout = findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(ArticlesActivity.this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.navigationViewID);
        navigationView.setNavigationItemSelectedListener(this);

        initViewPager();
        getPicture(0);


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initViewPager() {
        Log.i("INITTTT", "INITTTT");
        viewPager = findViewById(R.id.viewpager);
        slideAdapter = new SlideAdapters(this);
        viewPager.setAdapter(slideAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                getPicture(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.home:
                mDrawerLayout.closeDrawers();
                break;
            case R.id.settings:
                getUserData();
                break;
            case R.id.logOut:
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                break;
            case R.id.cart:
                Intent kartIntent = new Intent(ArticlesActivity.this, KosaricaActivity.class);
                startActivity(kartIntent);
                break;
        }

        return false;
    }


    public void getPicture(final int position) {

        if (singelton.getHmProducts().containsKey(position)) {
            if (singelton.getHmProducts().get(position).getSlika1() == null) {
                setImageViewNoPicture(position);
            } else {
                setImageView(position, singelton.getHmProducts().get(position));
            }

            setRatingStars(position, singelton.getHmProducts().get(position));
            setTextViews(position, singelton.getHmProducts().get(position));

            return;
        }

        Log.i("WAT", "position = " + position);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, singelton.getUrl() + "artikel-stranka-android/" + position, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            JSONObject jsonObject = (JSONObject) response.get(0);
                            int idIzdelka = Integer.parseInt(jsonObject.getString("id"));
                            Product product = new Product();
                            product.setProductID(idIzdelka);
                            product.setNaziv(jsonObject.getString("naziv"));
                            product.setFirma(jsonObject.getString("firma"));
                            product.setCena(jsonObject.getDouble("cena"));
                            product.setOpis(jsonObject.getString("opis"));
                            product.setKategorija(jsonObject.getString("kategorija"));
                            setBlob(idIzdelka, position, product);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HADUKEN NO", error.toString());

                    }
                });

        singelton.getQueue(ArticlesActivity.this).add(jsonArrayRequest);
    }

    public void setBlob(final int idIzdelka, final int position, final Product product) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, singelton.getUrl() + "slika-artikel-first/" + idIzdelka, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("reponse", response.toString());
                        try {
                            String blobData = response.getString("slika");
                            byte decodedString[] = Base64.decode(blobData.split(",")[1], Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            product.setSlika1(decodedByte);
                            getOcene(idIzdelka, position, product);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HADUKEN NO", error.toString());
                        getOcene(idIzdelka, position, product);
                    }
                });

        singelton.getQueue(ArticlesActivity.this).add(jsonObjectRequest);
    }

    public void getOcene(int idIzdelka, final int position, final Product product) {

        Log.i("WAT", "SM V GET OCENE");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, singelton.getUrl() + "ocena/" + idIzdelka, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        product.setOcena(calculateAvg(response));
                        singelton.getHmProducts().put(position, product);
                        if (product.getSlika1() == null) {
                            setImageViewNoPicture(position);
                        } else {
                            setImageView(position, product);
                        }
                        setRatingStars(position, product);
                        setTextViews(position, product);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HADUKEN NO", error.toString());

                    }
                });

        singelton.getQueue(ArticlesActivity.this).add(jsonArrayRequest);
    }

    private void setRatingStars(final int position, Product product) {
        View currentView = viewPager.findViewWithTag("myview" + position);
        RatingBar ratingBar = currentView.findViewById(R.id.ratingBar1);
        ratingBar.setRating(product.getOcena());
    }

    private void setImageView(final int position, final Product product) {
        View currentView = viewPager.findViewWithTag("myview" + position);

        ImageView imageView = currentView.findViewById(R.id.slideimg);
        imageView.setImageBitmap(product.getSlika1());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArticlesActivity.this, DetailsActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }

    private void setImageViewNoPicture(final int position) {
        View currentView = viewPager.findViewWithTag("myview" + position);
        ImageView imageView = currentView.findViewById(R.id.slideimg);
        imageView.setImageResource(R.drawable.icon);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArticlesActivity.this, DetailsActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

    }

    private void setTextViews(final int position, Product product) {
        View currentView = viewPager.findViewWithTag("myview" + position);

        TextView textViewTitle = currentView.findViewById(R.id.titleID);
        textViewTitle.setText(product.getNaziv());

        TextView textViewDesc = currentView.findViewById(R.id.price);
        textViewDesc.setText("" + product.getCena() + " €");
    }

    private static int calculateAvg(JSONArray jsonArray) {
        if(jsonArray.length() == 0){
            return 0;
        }
        int sum = 0;
        Log.i("sum", jsonArray.toString());

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                sum += jsonArray.getJSONObject(i).getInt("ocena");
                Log.i("sum", "" + sum);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        Log.i("sum", "rez = " + sum + "len = " + jsonArray.length());
        return sum / jsonArray.length();
    }

    public void getUserData(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, singelton.getUrl() + "stranka/" + singelton.getUserID(), null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            singelton.setIme(response.getString("ime"));
                            singelton.setPriimek(response.getString("priimek"));
                            singelton.setNaslov(response.getString("naslov"));
                            singelton.setTelefon(response.getString("telefon"));

                            Intent settingsIntent = new Intent(ArticlesActivity.this, ActivitySettings.class);
                            startActivity(settingsIntent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("userget", "ERROR");
                    }
                });

        singelton.getQueue(ArticlesActivity.this).add(jsonObjectRequest);
    }


}
