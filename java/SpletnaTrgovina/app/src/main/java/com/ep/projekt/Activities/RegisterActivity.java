package com.ep.projekt.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ep.projekt.R;
import com.ep.projekt.Singelton.Singelton;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private static Singelton singelton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        singelton = Singelton.getInstance();


        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Button btn = findViewById(R.id.button_register);

        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                EditText editTextEmail = findViewById(R.id.txtemail);
                EditText editeTextPassword = findViewById(R.id.txtpassword);
                canLogin(editTextEmail.getText().toString(), editeTextPassword.getText().toString());
            }
        });

    }

    public void canLogin(String email, String geslo){

        if(email.length() == 0 || geslo.length() == 0){
            Toast.makeText(RegisterActivity.this, "Login failed check your email or password", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("email", email);
            jsonBody.put("geslo", geslo);

            JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, singelton.getUrl()+"stranka/validate", jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String msg = response.getString("message");
                        String userID = response.getString("id");
                        if(msg.equals("Successfull.")){
                            Toast.makeText(RegisterActivity.this, "Login successful", Toast.LENGTH_LONG).show();
                            singelton.setUserID(Integer.parseInt(userID));
                            getNumberOfProducts();
                        }else{
                            Toast.makeText(RegisterActivity.this, "Login failed check your email or password", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(RegisterActivity.this, "Login failed check your email or password", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    return headers;
                }
            };

            singelton.getQueue(RegisterActivity.this).add(jsonOblect);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getNumberOfProducts(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, singelton.getUrl() + "artikel-stranka-android-count", null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("stIzdelkov", response.toString());
                        try {
                            int stIzdelkov = response.getInt("count");
                            Intent home = new Intent(RegisterActivity.this, ArticlesActivity.class);
                            singelton.setStIzdelkov(stIzdelkov);
                            startActivity(home);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HADUKEN NO", error.toString());

                    }
                });

        singelton.getQueue(RegisterActivity.this).add(jsonObjectRequest);
    }


}




