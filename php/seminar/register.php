<?php

    session_start();

    require_once 'checkCerts.php';

    $role = Checker::myRole();

    if(isset($_SESSION["id"])){
        header('Location: ' . str_replace("/register.php", "/shop/main.php", $_SERVER["PHP_SELF"]));
        die();
    }
    
    
    if($role !== "Stranka"){
        header('Location: ' . str_replace("/register.php", "/login.php", $_SERVER["PHP_SELF"]));
        die();
    }
    if(isset($_POST['submit'])){
        function CheckCaptcha($userResponse){
            $fields_string = '';
            $fields = array(
                'secret' => '6Lfwv4UUAAAAAAWaWeSltPXH83ske4X2z6pn2u74',
                'response' => $userResponse
            );

            foreach($fields as $key=>$value)
            $fields_string .= $key . '=' . $value . '&';
            $fields_string = rtrim($fields_string, '&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

            $res = curl_exec($ch);
            curl_close($ch);

            return json_decode($res, true);
        }
        
        $result = CheckCaptcha($_POST['g-recaptcha-response']);

        if($result['success']) {
            // captcha je uredu probamo registrirati uporabnika
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/stranka");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = json_decode(curl_exec($ch), true);

            curl_close($ch);

            if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                header('Location: ' . str_replace("/register.php", "/login.php", $_SERVER["PHP_SELF"]));
                die();
            }else{
                $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                header('Location: ' . $redirect);
                die();
            }
        }else{
            ?>
        <!DOCTYPE html>
            <html lang="en">
               <head>
                  <meta charset="UTF-8">
                  <meta http-equiv="X-UA-Compatible" content="ie=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <link rel="stylesheet" href="css/register.css">
                  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                  <title>Registration</title>
               </head>
               <body>
                  <div class="containerCustom">
                     <div class="signup-content">
                        <form method="POST" action="<?=$_SERVER["PHP_SELF"]?>" id="signup-form" class="signup-form" autocomplete="off">
                           <h3 class="form-title">Registration</h3>
                           <div class="alert alert-danger alert-dismissible show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Oh snap, </strong>please check your input.
                          </div>
                           <div class="form-group has-feedback">
                              <input type="text" class="form-input" minlength="3" maxlength="255" name="ime" placeholder="Name" required/>
                              <i class="glyphicon glyphicon-user form-control-feedback"></i>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="text" class="form-input" minlength="3" maxlength="255" name="priimek" placeholder="Surname" required/>
                              <i class="glyphicon glyphicon-tag form-control-feedback"></i>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="email" class="form-input" minlength="6" maxlength="255" name="email" id="email" placeholder="Email" required/>
                              <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                           </div>
                          <div class="form-group has-feedback">
                              <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" class="form-input" name="telefon" placeholder="Phone: 123-456-789" required/>
                              <i class="glyphicon glyphicon-phone form-control-feedback"></i>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="text" class="form-input" minlength="3" maxlength="255" name="naslov" placeholder="Address" required/>
                              <i class="glyphicon glyphicon-home form-control-feedback"></i>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="password" minlength="6" maxlength="255" class="form-input" name="geslo" id="Password" placeholder="Password" required/>
                              <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="password" minlength="6" maxlength="255" class="form-input" name="geslo_repeat" placeholder="Confrim password" required/>
                              <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                           </div>
                           <div class="g-recaptcha" data-sitekey="6Lfwv4UUAAAAAM1zCL7GiLDAjBiZ0z-W8y9MrxNb" style="margin-left:10px; margin-top:10px; margin-bottom:10px;"></div>
                           <input type="hidden" name="submit" value="submit" />
                           <div class="form-group">
                              <button type="submit" class="custom-button sign-up">Register</button>
                           </div>
                        </form>
                        <p class="login-paragraph">Already have an account?&nbsp;&nbsp;<a href="<?=str_replace("/register.php","/login.php", $_SERVER["PHP_SELF"])?>" class="login">Login</a></p>
                     </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                  <script src='https://www.google.com/recaptcha/api.js'></script>
               </body>
            </html>
        <?php
        }
    }else{
        ?>
        <!DOCTYPE html>
        <html lang="en">
           <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="ie=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <link rel="stylesheet" href="css/register.css">
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
              <title>Registration</title>
           </head>
           <body>
              <div class="containerCustom">
                 <div class="signup-content">
                    <form method="POST" action="<?=$_SERVER["PHP_SELF"]?>" id="signup-form" class="signup-form" autocomplete="off">
                       <h3 class="form-title">Registration</h3>
                       <div class="form-group has-feedback">
                          <input type="text" class="form-input" minlength="3" maxlength="255" name="ime" placeholder="Name" required/>
                          <i class="glyphicon glyphicon-user form-control-feedback"></i>
                       </div>
                       <div class="form-group has-feedback">
                          <input type="text" class="form-input" minlength="3" maxlength="255" name="priimek" placeholder="Surname" required/>
                          <i class="glyphicon glyphicon-tag form-control-feedback"></i>
                       </div>
                       <div class="form-group has-feedback">
                          <input type="email" class="form-input" minlength="6" maxlength="255" name="email" id="email" placeholder="Email" required/>
                          <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                       </div>
                      <div class="form-group has-feedback">
                          <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" class="form-input" name="telefon" placeholder="Phone: 123-456-789" required/>
                          <i class="glyphicon glyphicon-phone form-control-feedback"></i>
                       </div>
                       <div class="form-group has-feedback">
                          <input type="text" class="form-input" minlength="3" maxlength="255" name="naslov" placeholder="Address" required/>
                          <i class="glyphicon glyphicon-home form-control-feedback"></i>
                       </div>
                       <div class="form-group has-feedback">
                          <input type="password" minlength="6" maxlength="255" class="form-input" name="geslo" id="Password" placeholder="Password" required/>
                          <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                       </div>
                       <div class="form-group has-feedback">
                          <input type="password" minlength="6" maxlength="255" class="form-input" name="geslo_repeat" placeholder="Confrim password" required/>
                          <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                       </div>
                       <div class="g-recaptcha" data-sitekey="6Lfwv4UUAAAAAM1zCL7GiLDAjBiZ0z-W8y9MrxNb" style="margin-left:10px; margin-top:10px; margin-bottom:10px;"></div>
                       <input type="hidden" name="submit" value="submit" />
                       <div class="form-group">
                          <button type="submit" class="custom-button sign-up">Register</button>
                       </div>
                    </form>
                    <p class="login-paragraph">Already have an account?&nbsp;&nbsp;<a href="<?=str_replace("/register.php","/login.php", $_SERVER["PHP_SELF"])?>" class="login">Login</a></p>
                 </div>
              </div>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
              <script src='https://www.google.com/recaptcha/api.js'></script>
           </body>
        </html>
    <?php
    }
?>