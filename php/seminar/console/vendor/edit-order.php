<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/console/vendor/edit-order.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }
    
    require_once '../../checkCerts.php';
    
    $role = Checker::myRole();
    
    //ce je ROLE NULL ali razlicen od administrator potem mu ne dovolimo dostopa
    if(is_null($role) || $role !== "Prodajalec"){
        $newURL= str_replace("/console/vendor/edit-order.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }
    $id = 0;
    $wasPost = false;
    $change = false;
    
    if(isset($_POST["id"])){
        $_POST["datum_odobreno"] = date("d/m/Y");
        $_POST["Prodajalec_id"] = $_SESSION["id"];
        $id = $_POST["id"];
        $wasPost = true;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/narocilo/" . $_POST["id"]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = json_decode(curl_exec($ch), true);

        curl_close($ch);

        if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
            $change = true;
        }
        
    }else if(!isset($_GET["id"])){
        die();
    }else{
        $id = $_GET["id"];
    }
    
    // si admin in lahko dostopas do console
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/narocilo/" . $id);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = json_decode(curl_exec($ch), true);
    curl_close($ch);

    if(isset($output["error"]) || empty($output)){
        var_dump($output);
        die();
    }
    
   /* $ime = $output["ime"];
    $priimek = $output["priimek"];
    $email = $output["email"];
    $statusRacuna = $output["statusRacuna"];
    $telefon = $output["telefon"];
    $naslov = $output["naslov"];
    $who = $ime . " " . $priimek;
   */
   $postLocation = $_SERVER["PHP_SELF"] . "?id=" . $id;
   $status = $output[0]["status"];
   $datumOddano = $output[0]["datum_oddano"];
   $datumOdobreno = $output[0]["datum_odobreno"];
   $skupnaCena = $output[0]["skupnaCena"];
   $ime = $output[0]["ime"];
   $priimek = $output[0]["priimek"];
   $quantity = $output[0]["quantity"];
   $who = $ime . " " . $priimek;
    
    ?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Edit order</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/css/Navigation-Clean.css">
      <link rel="stylesheet" type="text/css" href="../../css/profileSettings.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-default navigation-clean">
            <div class="container">
               <div class="navbar-header">
                  <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                  <p class="navbar-brand">Online shop</p>
               </div>
               <div class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-order.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-order.php", "/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-order.php", "/products.php", $_SERVER["PHP_SELF"])?>">Products</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-order.php", "/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-order.php", "/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-order.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="bar"></div>
      <div class="container" style="margin-bottom: 100px;">
         <h2 class="h2-name"><?=$who?></h2>
         <img src="https://www.kronos.mx/sites/default/files/styles/list_square/public/images/reference/Kronos-product-icons_400px_0000s_0004_Production-Order-Tracking.png?itok=MmwYfWFQ" id="profileSettingsAvatar" style="border-radius: 0px;" alt="Order_Icon.png">
         <?php
            if($wasPost){
                if($change){ ?>
                  <div class="alert alert-success alert-dismissible show" role="alert" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Success, </strong>the data was changed.
                  </div> <?php
                }else{ ?>
                  <div class="alert alert-danger alert-dismissible show" role="alert" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Oh snap, </strong>please check your input.
                  </div> 
                <?php    
                }
            }
         ?>
         
         <div class="form-group">
            <label>Customer</label>
            <input class="form-control" type="text" placeholder="Customer" value="<?=$who?>" disabled>
        </div>
        <div class="form-group">
            <label>Status</label>
            <input class="form-control" type="text" placeholder="Status" value="<?=$status?>" disabled>
        </div>
         <div class="form-group">
            <label>Date added</label>
            <input class="form-control" type="text" placeholder="Date added" value="<?=$datumOddano?>" disabled>
        </div>
        <div class="form-group">
            <label>Date accepted</label>
            <input class="form-control" type="text" placeholder="Date accepted" value="<?=$datumOdobreno?>" disabled>
        </div>
        <label>Products</label><br/>
        <?php foreach ($output as $keyItem => $keyValue){ ?>
        
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Name" style="width:30%; display:inline;" value="<?=$keyValue["naziv"]?>" disabled>
                    <input class="form-control" type="text" placeholder="Quantity" style="width:15%; display:inline;" value="<?=$keyValue["quantity"]?>" disabled>
                    <input class="form-control" type="text" placeholder="Price per Item" style="width:15%; display:inline;" value="<?=$keyValue["quantity"] * $keyValue["cena"]?>€" disabled>
                </div>
        <?php } ?>
        <div class="form-group">
            <label>Total price</label>
            <input class="form-control" type="text" placeholder="Total price" value="<?=$skupnaCena?>" disabled>
        </div>
         <form action="<?=$postLocation?>" method="post">
            <input type="hidden" name="id" value="<?=$id?>">
             <label for="status">Order status</label>
             <select class="form-control" name="status">
               <option>accepted</option>
               <option>refused</option>
             </select>
            <div class="text-center">
               <button class="btn btn-primary" type="submit" class="btn-post" style="margin-top: 30px;">Update order</button>
            </div>
         </form>
      </div>
      <script src="../../assets/js/jquery.min.js"></script>
      <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
   </body>
</html>