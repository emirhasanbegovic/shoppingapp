<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/console/vendor/edit-product-pictures.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }
    
    require_once '../../checkCerts.php';
    
    $role = Checker::myRole();
    
    //ce je ROLE NULL ali razlicen od administrator potem mu ne dovolimo dostopa
    if(is_null($role) || $role !== "Prodajalec"){
        $newURL= str_replace("/console/vendor/edit-product-pictures.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }
    $id = 0;
    $wasPost = false;
    $change = false;
    
    if(isset($_POST["id"])){
        /*$id = $_POST["id"];
        $wasPost = true;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/stranka/" . $id);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = json_decode(curl_exec($ch), true);
        //$output = curl_exec($ch);
        curl_close($ch);

        if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
            $change = true;
        }*/
        
    }else if(!isset($_GET["id"])){
        die();
    }else{
        $id = $_GET["id"];
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/slika-artikel/" . $id);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = json_decode(curl_exec($ch), true);

    
    curl_close($ch);
    
    if(isset($output["error"])){
        //var_dump($output);
        die();
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel/" . $id);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $outputArtikel = json_decode(curl_exec($ch), true);

    
    curl_close($ch);
    
    if(isset($outputArtikel["error"])){
        //var_dump($outputArtikels);
        die();
    }
    
    ?>

<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Edit product pictures</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/css/Navigation-Clean.css">
      <link rel="stylesheet" type="text/css" href="../../css/edit-product-pictures.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-default navigation-clean">
            <div class="container">
               <div class="navbar-header">
                  <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                  <p class="navbar-brand">Online shop</p>
               </div>
               <div class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-product-pictures.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-product-pictures.php", "/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-product-pictures.php", "/products.php", $_SERVER["PHP_SELF"])?>">Products</a></li>
                     <li role="presentation"><a href="<?=str_replace("/edit-product-pictures.php", "/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-product-pictures.php", "/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                           <li role="presentation"><a href="<?=str_replace("/console/vendor/edit-product-pictures.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="bar"></div>
      <div class="container" style="margin-top:30px;">
        <div class="row row-fix-top">
            <?php
            foreach($output as $key => $value){     
            ?>
                <div class="col-lg-4 col-sm-6 edit2 tOverflow" onclick="this.firstElementChild.click()">
                   <input type="file" style="display:none;" onchange="imageUpdate(this)">
                   <input type="hidden" value="<?=$value["id"]?>">
                   <a href="#" onclick="event.stopPropagation(); imageRemove(this);"><button class="btn btn-default" style="position:absolute; color:red; margin-top: 10px;"><b>X</b></button></a>
                   <h3 class="text-center"><?=$outputArtikel["naziv"]?></h3>
                   <img class="avatar" src="<?=$value["slika"]?>" width="200" height="200" alt="Product.png">
                </div>
            <?php
              }    
            ?>
            <div class="col-lg-4 col-sm-6 edit2 tOverflow" onclick="this.firstElementChild.click();">
               <input type="file" style="display:none;" onchange="imageUploadNew(this)">
               <h3 class="text-center">Add picture</h3>
               <img class="avatar" src="../../images/Dropbox-icon.png" width="200" height="200" alt="Product.png">
            </div>
        </div>
      </div>
      <script>
	function imageUploadNew(e){
		var reader = new FileReader();

		var file = e.files[0];
		var fileName = e.files[0].name;
                var url = window.location.pathname;
                var id = window.location.search;
                id = id.split("?id=")[1];
                        
		url = url.replace("/seminar/console/vendor/edit-product-pictures.php","/REST-API/api/slika");
	
		reader.addEventListener("load", function(){
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onreadystatechange = function(){
				if(this.readyState === XMLHttpRequest.DONE && this.status == 201){
                                    window.location.reload();
				}else if(this.readyState === XMLHttpRequest.DONE){
                                    alert("Something went wrong. File size probably exceeds the server limit.");
				}
				
			}

			var obj = {
				Artikel_id: id,
				slika: reader.result
			}

			xhr.send(JSON.stringify(obj));

		});

		if(file){
                    reader.readAsDataURL(file);
		}
	}
        	
        function imageUpdate(e){
		var reader = new FileReader();

		var file = e.files[0];
		var fileName = e.files[0].name;
                var url = window.location.pathname;
                console.log(e);
                var id = e.nextElementSibling.value;
     
		url = url.replace("/seminar/console/vendor/edit-product-pictures.php","/REST-API/api/slika"+"/"+id);
	
		reader.addEventListener("load", function(){
			var xhr = new XMLHttpRequest();
			xhr.open("PUT", url, true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onreadystatechange = function(){
				if(this.readyState === XMLHttpRequest.DONE && this.status == 200){
                                    window.location.reload();
				}else if(this.readyState === XMLHttpRequest.DONE){
                                    alert("Something went wrong. File size probably exceeds the server limit.");
				}
				
			}

                        var idA = window.location.search;
                        idA = idA.split("?id=")[1];

			var obj = {
				Artikel_id: idA,
				slika: reader.result
			}

			xhr.send(JSON.stringify(obj));

		});

		if(file){
                    reader.readAsDataURL(file);
		}
	}
        
        function imageRemove(e){
                var url = window.location.pathname;
                var id = e.previousElementSibling.value;
		url = url.replace("/seminar/console/vendor/edit-product-pictures.php","/REST-API/api/slika")+"/"+id;

                var xhr = new XMLHttpRequest();
                xhr.open("DELETE", url, true);
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.onreadystatechange = function(){
                        if(this.readyState === XMLHttpRequest.DONE && this.status == 200){
                                window.location.reload();
                        }else if(this.readyState === XMLHttpRequest.DONE){
                                alert("Something went wrong. File could not be deleted.");
                        }

                }

                xhr.send(null);
	}
      </script>
      <script src="../../assets/js/jquery.min.js"></script>
      <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
   </body>
</html>
