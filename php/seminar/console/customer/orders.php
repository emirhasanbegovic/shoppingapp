<?php

    session_start();
    
    if(!isset($_SESSION["id"])){
        $newURL= str_replace("/console/customer/orders.php","/login.php", $_SERVER["REQUEST_URI"]);
        header('Location: '.$newURL);
        die();
    }

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
            $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            header('Location: ' . $redirect);
            exit();
        }

        require_once '../../checkCerts.php';

        $role = Checker::myRole();

        //ce je ROLE NULL ali razlicen od Prodajalec potem mu ne dovolimo dostopa
        if(is_null($role) || $role !== "Stranka"){
            $newURL= str_replace("/console/customer/orders.php","/login.php", $_SERVER["REQUEST_URI"]);
            header('Location: '.$newURL);
            die();
        }

        // si stranka in lahko dostopas do console
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/narocilo-stranka/" . $_SESSION["id"]);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);

        curl_close($ch);
    ?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Orders</title>
      <link rel="stylesheet" type="text/css" href="../../css/admin-console.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/css/Navigation-Clean.css">
      <!-- Font Awesome Icon Library -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-default navigation-clean">
            <div class="container">
               <div class="navbar-header">
                  <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                  <p class="navbar-brand">Online shop</p>
               </div>
               <div class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav navbar-right">
                     <li role="presentation"><a href="#kartModal" id="cart" onclick="generateTableFromCookie()" data-toggle="modal"><i class="fa fa-shopping-cart"></i> Cart </a></li>
                     <li role="presentation"><a href="<?=str_replace("/console/customer/orders.php", "/shop/main.php", $_SERVER["PHP_SELF"])?>">Shop</a></li>
                     <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Orders</a></li>
                     <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li role="presentation"><a href="<?=str_replace("/console/customer/orders.php", "/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                           <li role="presentation"><a href="<?=str_replace("/console/customer/orders.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <div class="bar"></div>
      <!-- Modal -->
            <div class="modal fade" id="kartModal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><b>My Cart</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form>
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Number of items</th>
                                    <th scope="col">Price</th>
                                    <th scope="col"></th>
                                 </tr>
                              </thead>
                              <tbody id="modaltablebody">
                              </tbody>
                           </table>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success" style="float:center;" data-dismiss="modal" id="confirmButton" onclick="confirmButton()">Confirm</button>
                        <button type="button" class="btn btn-danger" id="deletAllItems" style="float: left; padding-top:5px; padding-bottom:5px;" onclick="deleteAllItems()"><span  style="float: left; padding-top:5px; padding-bottom:5px;" class='glyphicon glyphicon-trash'></button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="racunModal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">My cart</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form>
                           <h3 class="trenutniDatum"> 01/02/2009</h3>
                           <div class="panel panel-default" style="margin-top: 25px;">
                              <div class="panel-heading">
                                 <h3 class="panel-title"><strong>Cart confirmation</strong></h3>
                                 <div class="panel-body">
                                    <div class="table-responsive">
                                       <table class="table table-condensed">
                                          <thead>
                                             <tr>
                                                <td><strong>Item</strong></td>
                                                <td class="text-center"><strong>Price</strong></td>
                                                <td class="text-center"><strong>Quantity</strong></td>
                                                <td class="text-right"><strong>Totals</strong></td>
                                             </tr>
                                          </thead>
                                          <tbody id="racunBody">
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="sendButton" onclick="sendButton()">Send</button>
                        <button type="button" class="btn btn-secondary" style="float: left;" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
              <div class="container" style="margin-bottom: 20px;">
            <div class="row row-fix">
               <div class="col-md-12">
                  <h1 class="bolded">Orders</h1>
                  <h2 class="bolded h1-titles">Date added | Date accepted | Total price | Status</h2>
               </div>
            </div>
        </div>
      <?php
        if(empty($output)){
            ?>
                <div class="container" style="margin-top:10px;">
                   <div class="row row-fix">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 details">
                          <p class="input-b" name="location">Empty</p>
                      </div>
                   </div>
                </div>
      <?php
        }else{
            foreach($output as $key=>$value){
                if(is_null($value["datum_odobreno"])){
                    $value["datum_odobreno"] = "PENDING";
                }
                $who = $value["datum_oddano"] . " | <b>" . $value["datum_odobreno"] . "</b> | " . $value["skupnaCena"] . " | <b>" . $value["status"] . "</b>";
                ?>
                <div class="container" style="margin-top:10px;">
                    <div class="row row-fix">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 details">
                           <p class="input-b" name="location"><?=$who?></p>
                          <a href="<?=str_replace("/orders.php","/preview-order.php?id=" . $value["id"], $_SERVER["PHP_SELF"])?>"><button class="edit2"><i class="glyphicon glyphicon-pencil"></i></button></a>
                       </div>
                    </div>
                </div>
                <?php
            }
        }  
      ?>
      <script src="../../assets/js/jquery.min.js"></script>
      <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="../../assets/js/shoppingCart.js"></script>
   </body>
</html>