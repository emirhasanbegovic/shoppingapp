<?php
    session_start();
    $anonimenodjemalec = !isset($_SESSION["id"]); 
    
    // si prijavljen ampak nimas HTTPS
    if((empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") && !$anonimenodjemalec){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }

    require_once '../checkCerts.php';
    
    $role = Checker::myRole();

    $offsetPagination = 0;
    $offset = 0;
    
    if(isset($_GET["offset"]) && is_int((int)$_GET["offset"])){
        $offset = ((int)$_GET["offset"]) * 6;
        $offsetPagination = ((int)$_GET["offset"]);
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/artikel-stranka/" . $offset);
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json'
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = json_decode(curl_exec($ch), true);
    
    curl_close($ch);
    
    if(is_null($output) || isset($output["error"])){
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Shop</title>
      <link rel="stylesheet" type="text/css" href="../css/main.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="../assets/css/Navigation-Clean.css">
      <!-- Font Awesome Icon Library -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
       
       <?php
            if($role === "Stranka" && !$anonimenodjemalec){ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="#kartModal" id="cart" onclick="generateTableFromCookie()" data-toggle="modal"><i class="fa fa-shopping-cart"></i> Cart </a></li>
                            <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/console/customer/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                            <li class="dropdown">
                               <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                               <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a href="<?=str_replace("/shop/main.php","/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                                  <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                               </ul>
                            </li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
            <!-- Modal -->
            <div class="modal fade" id="kartModal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><b>My Cart</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form>
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Number of items</th>
                                    <th scope="col">Price</th>
                                    <th scope="col"></th>
                                 </tr>
                              </thead>
                              <tbody id="modaltablebody">
                              </tbody>
                           </table>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success" style="float:center;" data-dismiss="modal" id="confirmButton" onclick="confirmButton()">Confirm</button>
                        <button type="button" class="btn btn-danger" id="deletAllItems" style="float: left; padding-top:5px; padding-bottom:5px;" onclick="deleteAllItems()"><span  style="float: left; padding-top:5px; padding-bottom:5px;" class='glyphicon glyphicon-trash'></button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="racunModal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">My cart</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <form>
                           <h3 class="trenutniDatum"> 01/02/2009</h3>
                           <div class="panel panel-default" style="margin-top: 25px;">
                              <div class="panel-heading">
                                 <h3 class="panel-title"><strong>Cart confirmation</strong></h3>
                                 <div class="panel-body">
                                    <div class="table-responsive">
                                       <table class="table table-condensed">
                                          <thead>
                                             <tr>
                                                <td><strong>Item</strong></td>
                                                <td class="text-center"><strong>Price</strong></td>
                                                <td class="text-center"><strong>Quantity</strong></td>
                                                <td class="text-right"><strong>Totals</strong></td>
                                             </tr>
                                          </thead>
                                          <tbody id="racunBody">
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="sendButton" onclick="sendButton()">Send</button>
                        <button type="button" class="btn btn-secondary" style="float: left;" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
     <?php   }else if($role === "Administrator" && !$anonimenodjemalec){ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/main.php","/console/admin.php", $_SERVER["PHP_SELF"])?>">Admin console</a></li>
                            <li class="dropdown">
                               <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                               <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a href="<?=str_replace("/shop/main.php","/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                                  <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                               </ul>
                            </li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
        <?php }else if($role === "Prodajalec" && !$anonimenodjemalec){ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/console/vendor/customer.php", $_SERVER["PHP_SELF"])?>">Shopper console</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/console/vendor/products.php", $_SERVER["PHP_SELF"])?>">Products</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/console/vendor/orders.php", $_SERVER["PHP_SELF"])?>">Orders</a></li>
                            <li class="dropdown">
                               <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Settings <span class="caret"></span></a>
                               <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a href="<?=str_replace("/shop/main.php","/account/settings.php", $_SERVER["PHP_SELF"])?>">Account</a></li>
                                  <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/logout.php", $_SERVER["PHP_SELF"])?>">Logout</a></li>
                               </ul>
                            </li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
        <?php }else{ ?>
             <div>
                <nav class="navbar navbar-default navigation-clean">
                   <div class="container">
                      <div class="navbar-header">
                         <button class="navbar-toggle collapsed menu-button" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                         <p class="navbar-brand">Online shop</p>
                      </div>
                      <div class="collapse navbar-collapse" id="navcol-1">
                         <ul class="nav navbar-nav navbar-right">
                            <li role="presentation"><a href="<?=$_SERVER["PHP_SELF"]?>">Shop</a></li>
                            <li role="presentation"><a href="<?=str_replace("/shop/main.php", "/register.php", $_SERVER["PHP_SELF"])?>">Register</a></li>
                         </ul>
                      </div>
                   </div>
                </nav>
             </div>
     <?php  }  ?>
        <div class="bar"></div>  
             <div class="container">
      <div class="text-center">
          <h2>All products</h2>
      </div>
                 
     <?php
        $offsetPagination = $offsetPagination + 1; // zacnemo z 0
        $previousLink = 0;
        $currentLink = 1;
        $nextLink = 2;
        
        if($offsetPagination <= 1){
            $offsetPagination = 1;
            $nextLink = 3;
        }else{
            $previousLink = $offsetPagination - 1;
            $currentLink = $offsetPagination;
            $nextLink = $offsetPagination + 1;         
        } 
        ?>
        
      <nav aria-label="...">
         <div class="text-center">
            <ul class="pagination">
               <?php if($currentLink > 1){ ?> <li class="page-item"><a class="page-link" href="<?=$_SERVER["PHP_SELF"] . "?offset=" . ($previousLink-1)?>">Previous</a></li><?php } ?>
               
               <?php if($currentLink == 1){ ?> <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li><?php } ?>
               <?php if($currentLink > 1){ ?> <li class="page-item "><a class="page-link" href="<?=$_SERVER["PHP_SELF"] . "?offset=" . ($previousLink-1)?>"><?=$previousLink?></a></li><?php } ?>
               
               <?php if($currentLink == 1){ ?> <li class="page-item "><a class="page-link" href="<?=$_SERVER["PHP_SELF"] . "?offset=" . $currentLink?>">2</a></li><?php } ?>
               <?php if($currentLink > 1){ ?> <li class="page-item active"><a class="page-link" href="<?=$_SERVER["PHP_SELF"] . "?offset=" . ($currentLink-1)?>"><?=$currentLink?> <span class="sr-only">(current)</span></a></li><?php } ?>
               
               <?php if(!empty($output)){ ?>  <li class="page-item "><a class="page-link" href="<?=$_SERVER["PHP_SELF"] . "?offset=" . ($nextLink-1)?>"><?=$nextLink?></a></li>
                                             <li class="page-item"><a class="page-link" href="<?=$_SERVER["PHP_SELF"] . "?offset=" . $currentLink?>">Next</a></li>          <?php } ?>
            </ul>
         </div>
      </nav>
                 
                 
                 
        <?php
        
     ?>
                
      <div id="content">
            <?php
                foreach ($output as $key => $value) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/slika-artikel-first/" . $value["id"]);
                    $headers = array(
                        'Accept: application/json',
                        'Content-Type: application/json'
                    );

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $outputSlika = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "localhost/netbeans/REST-API/api/ocena/" . $value["id"]);
                    $headers = array(
                        'Accept: application/json',
                        'Content-Type: application/json'
                    );

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $outputOcena = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    $ocena = 0;
                    $numberOfOcena = 0;
                    if(isset($outputOcena["error"]) || empty($outputOcena)){
                        $ocena = 0;
                        $numberOfOcena = 0;
                    }else{
                        foreach ($outputOcena as $ocenaKey => $ocenaValue) {
                            $ocena = $ocena + $ocenaValue["ocena"];
                            $numberOfOcena = $numberOfOcena + 1;
                        }
                        $ocena = (int)($ocena/(int)$numberOfOcena);
                    }
                    
                    
                    // izdelek nima nastavljene slike
                    if(isset($outputSlika["error"])){ ?>
                     <div class="col-lg-4 col-sm-6 edit2 tOverflow" onclick="location.href='<?=str_replace("/main.php", "/details.php?id=" . $value["id"], $_SERVER["PHP_SELF"])?>'">
                        <h3 class="title"><?=$value["naziv"]?></h3>
                        <img class="avatar" src="../images/Dropbox-icon.png" width="150" height="150" alt="Product.png">
                        <?php
                            for ($i = 1; $i <= $ocena; $i++) { ?>
                                <span class="fa fa-star checked"></span>
                        <?php } ?>
                        <?php
                            for ($i = $ocena; $i < 5; $i++) { ?>
                                <span class="fa fa-star"></span>
                        <?php } ?>
                        <p><b>Firm: <?=$value["firma"]?></b><br>Price: <?=$value["cena"]?></p>
                     </div>
          <?php     }else{ ?>
                     <div class="col-lg-4 col-sm-6 edit2 tOverflow" onclick="location.href='<?=str_replace("/main.php", "/details.php?id=" . $value["id"], $_SERVER["PHP_SELF"])?>'">
                        <h3 class="title"><?=$value["naziv"]?></h3>
                        <img class="avatar" src="<?=$outputSlika["slika"]?>" width="150" height="150" alt="Product.png">
                       <?php
                            for ($i = 1; $i <= $ocena; $i++) { ?>
                                <span class="fa fa-star checked"></span>
                        <?php } ?>
                        <?php
                            for ($i = $ocena; $i < 5; $i++) { ?>
                                <span class="fa fa-star"></span>
                        <?php } ?>
                        <p><b>Firm: <?=$value["firma"]?></b><br>Price: <?=$value["cena"]?></p>
                     </div>
           <?php    }
                }

            ?>
    </div>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <?php if($role === "Stranka"){ ?>
            <script src="../assets/js/shoppingCart.js"></script>
    <?php } ?>
   </body>
</html>