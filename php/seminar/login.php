<?php

session_start();

    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('Location: ' . $redirect);
        exit();
    }

    require_once 'checkCerts.php';
    
    $role = Checker::myRole();
    
    //ce je ROLE NULL potem ni autoriziran
    if(is_null($role)){
        die();
    }

    if(isset($_SESSION["id"])){
        $newURL='shop/main.php';
        header('Location: '.$newURL);
    }

    if(isset($_POST['submit'])){
        if(isset($_POST['email']) && isset($_POST['geslo'])){
            $url = "";
            //preverimo ce je login uredu
            if($role === "Stranka"){
                $url = "localhost/netbeans/REST-API/api/stranka/validate";
            }else if($role === "Prodajalec"){
                $url = "localhost/netbeans/REST-API/api/prodajalec/validate";
            }else if($role === "Administrator"){
                // admin login
                $url = "localhost/netbeans/REST-API/api/admin/validate";  
            }else{
                die();
            }
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = json_decode(curl_exec($ch), true);

            curl_close($ch);

            if(isset($output["message"]) && strcmp($output["message"], "Successfull.") == 0){
                $_SESSION["id"] = $output["id"];
            }else{
                $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                header('Location: ' . $redirect);
                die();
            }
            $newURL='shop/main.php';
            header('Location: '.$newURL);
            die();
        }else{
            ?>
                <!DOCTYPE html>
                    <html lang="en">
                       <head>
                          <meta charset="UTF-8">
                          <meta http-equiv="X-UA-Compatible" content="ie=edge">
                          <meta name="viewport" content="width=device-width, initial-scale=1.0">
                          <link rel="stylesheet" href="css/login.css">
                          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                          <title>Login</title>
                       </head>
                       <body>
                          <div class="containerCustom">
                             <div class="login-content">
                                <form method="post" action="<?=$_SERVER["PHP_SELF"]?>" id="login-form">
                                   <h3 class="form-title">Login</h3>
                                   <div class="alert alert-danger alert-dismissible show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Oh snap, </strong>wrong email or password.
                                   </div>
                                   <div class="form-group has-feedback">
                                      <input type="email" class="form-input" name="email" placeholder="Email" required/>
                                      <i class="glyphicon glyphicon-user form-control-feedback"></i>
                                   </div>
                                   <div class="form-group has-feedback">
                                      <input type="password" class="form-input" name="geslo" placeholder="Password" required/>
                                      <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                                   </div>
                                   <input type="hidden" name="submit" value="submit" />
                                   <div class="form-group">
                                      <button type="submit" class="custom-button btn-login">Login</button>
                                   </div>
                                </form>
                                <p class="login-paragraph">Don't have an account?&nbsp;&nbsp;<a href="<?=str_replace("/login.php","/register.php", $_SERVER["PHP_SELF"])?>" class="login">Sign up</a></p>
                             </div>
                          </div>
                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                       </body>
                    </html>
            <?php
        }
    }else{
       ?> 
          <!DOCTYPE html>
            <html lang="en">
               <head>
                  <meta charset="UTF-8">
                  <meta http-equiv="X-UA-Compatible" content="ie=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <link rel="stylesheet" href="css/login.css">
                  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                  <title>Login</title>
               </head>
               <body>
                  <div class="containerCustom">
                     <div class="login-content">
                        <form method="post" action="<?=$_SERVER["PHP_SELF"]?>" id="login-form">
                           <h3 class="form-title">Login</h3>
                           <div class="form-group has-feedback">
                              <input type="email" class="form-input" name="email" placeholder="Email" required/>
                              <i class="glyphicon glyphicon-user form-control-feedback"></i>
                           </div>
                           <div class="form-group has-feedback">
                              <input type="password" class="form-input" name="geslo" placeholder="Password" required/>
                              <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                           </div>
                           <input type="hidden" name="submit" value="submit" />
                           <div class="form-group">
                              <button type="submit" class="custom-button btn-login">Login</button>
                           </div>
                        </form>
                        <p class="login-paragraph">Don't have an account?&nbsp;&nbsp;<a href="<?=str_replace("/login.php","/register.php", $_SERVER["PHP_SELF"])?>" class="login">Sign up</a></p>
                     </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
               </body>
            </html>
        <?php
    }

?>