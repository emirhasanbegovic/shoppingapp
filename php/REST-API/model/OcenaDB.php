<?php

require_once 'model/AbstractDB.php';

class OcenaDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO Ocena (Stranka_id, Artikel_id, ocena, opis)"
                        . " VALUES (:Stranka_id, :Artikel_id, :ocena, :opis)", $params);
    }

    public static function update(array $params) {
        throw new Exception(); // ni mozno posodobiti ocene
    }

    public static function delete(array $id) {
        throw new Exception(); // ni jih mozno brisati
    }

    public static function get(array $id) {
        throw new Exception(); // ni mozno individualno oceno dobiti
    }
    
    public static function getAll() {
        throw new Exception(); // ni mozno dobiti vseh ocen
    }

    public static function getAllOceneByArtikelID($data) {
        return parent::query("SELECT o.Stranka_id, o.Artikel_id, o.ocena, o.opis, s.ime"
                        . " FROM Artikel a, Ocena o, Stranka s"
                        . " WHERE a.id = o.Artikel_id AND o.Stranka_id = s.id AND a.id = :Artikel_id", $data);
    }
}
