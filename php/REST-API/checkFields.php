<?php

class Checker {
    public static function CheckStrankaFields($data){
        
        if (isset($data['ime']) && strlen($data['ime']) < 255 && strlen($data['ime']) >= 3 &&
            isset($data['priimek']) && strlen($data['priimek']) < 255 && strlen($data['priimek']) >= 3 &&
            isset($data['email']) && strlen($data['email']) < 255 && strpos($data['email'], '@') !== false &&
            isset($data['naslov']) && strlen($data['naslov']) < 255 && strlen($data['naslov']) >= 3 &&
            isset($data['geslo']) && strlen($data['geslo']) < 255 && strlen($data['geslo']) > 5  &&
            isset($data['geslo_repeat']) && $data['geslo_repeat'] === $data['geslo'] && 
            isset($data['telefon']) && strlen($data['telefon']) == 11) {
            
                $newData['ime'] = htmlspecialchars($data['ime'], ENT_QUOTES, 'UTF-8');
                $newData['priimek'] = htmlspecialchars($data['priimek'], ENT_QUOTES, 'UTF-8');
                $newData['email'] = htmlspecialchars($data['email'], ENT_QUOTES, 'UTF-8');
                $newData['naslov'] = htmlspecialchars($data['naslov'], ENT_QUOTES, 'UTF-8');
                $newData['geslo'] = htmlspecialchars($data['geslo'], ENT_QUOTES, 'UTF-8');
                $newData['telefon'] = htmlspecialchars($data['telefon'], ENT_QUOTES, 'UTF-8');
                
                if(isset($data['statusRacuna'])){
                    $newData['statusRacuna'] = htmlspecialchars($data['statusRacuna'], ENT_QUOTES, 'UTF-8');
                }else{
                    $newData['statusRacuna'] = "active";
                }
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckStrankaPassword($data){
        
        if (isset($data['staro_geslo']) && strlen($data['staro_geslo']) < 255 && strlen($data['staro_geslo']) > 5  &&
            isset($data['novo_geslo']) && strlen($data['novo_geslo']) < 255 && strlen($data['novo_geslo']) > 5  &&
            isset($data['novo_geslo_repeat']) && strcmp($data['novo_geslo_repeat'], $data['novo_geslo']) == 0) {
            
                $newData['staro_geslo'] = htmlspecialchars($data['staro_geslo'], ENT_QUOTES, 'UTF-8');
                $newData['novo_geslo'] = htmlspecialchars($data['novo_geslo'], ENT_QUOTES, 'UTF-8');
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckStrankaInfo($data){
        if (isset($data['ime']) && strlen($data['ime']) < 255 && strlen($data['ime']) >= 3 &&
            isset($data['priimek']) && strlen($data['priimek']) < 255 && strlen($data['priimek']) >= 3 &&
            isset($data['naslov']) && strlen($data['naslov']) < 255 && strlen($data['naslov']) >= 3 &&
            isset($data['telefon']) && strlen($data['telefon']) == 11) {
            
                $newData['ime'] = htmlspecialchars($data['ime'], ENT_QUOTES, 'UTF-8');
                $newData['priimek'] = htmlspecialchars($data['priimek'], ENT_QUOTES, 'UTF-8');
                $newData['naslov'] = htmlspecialchars($data['naslov'], ENT_QUOTES, 'UTF-8');
                $newData['telefon'] = htmlspecialchars($data['telefon'], ENT_QUOTES, 'UTF-8');
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckAdminFields($data){
        
        if(isset($data['ime']) && strlen($data['ime']) < 255 && strlen($data['ime']) >= 3 &&
           isset($data['priimek']) && strlen($data['priimek']) < 255 && strlen($data['priimek']) >= 3 &&
           isset($data['email']) && strlen($data['email']) < 255 && strpos($data['email'], '@') !== false &&
           isset($data['geslo']) && strlen($data['geslo']) < 255 && strlen($data['geslo']) >= 3 && 
           isset($data['geslo_novo']) && strlen($data['geslo_novo']) < 255 && strlen($data['geslo_novo']) >= 3 &&
           isset($data['geslo_novo_repeat']) && strcmp($data['geslo_novo'], $data['geslo_novo_repeat']) == 0){
            
                $newData['ime'] = htmlspecialchars($data['ime'], ENT_QUOTES, 'UTF-8');
                $newData['priimek'] = htmlspecialchars($data['priimek'], ENT_QUOTES, 'UTF-8');
                $newData['email'] = htmlspecialchars($data['email'], ENT_QUOTES, 'UTF-8');
                $newData['geslo'] = htmlspecialchars($data['geslo'], ENT_QUOTES, 'UTF-8');
                $newData['geslo_novo'] = htmlspecialchars($data['geslo_novo'], ENT_QUOTES, 'UTF-8');
                
                return $newData;          
        }
        
        return NULL;
    }
    
    public static function CheckProdajalecFields($data){
        
        if (isset($data['ime']) && strlen($data['ime']) < 255 && strlen($data['ime']) >= 3 &&
            isset($data['priimek']) && strlen($data['priimek']) < 255 && strlen($data['priimek']) >= 3 &&
            isset($data['email']) && strlen($data['email']) < 255 && strpos($data['email'], '@') !== false &&          
            isset($data['geslo']) && strlen($data['geslo']) < 255 && strlen($data['geslo']) > 5 &&
            isset($data['geslo_repeat']) && $data['geslo_repeat'] === $data['geslo']) {
            
                $newData['ime'] = htmlspecialchars($data['ime'], ENT_QUOTES, 'UTF-8');
                $newData['priimek'] = htmlspecialchars($data['priimek'], ENT_QUOTES, 'UTF-8');
                $newData['email'] = htmlspecialchars($data['email'], ENT_QUOTES, 'UTF-8');
                $newData['geslo'] = htmlspecialchars($data['geslo'], ENT_QUOTES, 'UTF-8');
                $newData['geslo_repeat'] = htmlspecialchars($data['geslo_repeat'], ENT_QUOTES, 'UTF-8');
                
                if(isset($data['statusRacuna'])){
                    $newData['statusRacuna'] = htmlspecialchars($data['statusRacuna'], ENT_QUOTES, 'UTF-8');
                }else{
                    $newData['statusRacuna'] = "active";
                }
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckProdajalecInfo($data){
        if (isset($data['ime']) && strlen($data['ime']) < 255 && strlen($data['ime']) >= 3 &&
            isset($data['priimek']) && strlen($data['priimek']) < 255 && strlen($data['priimek']) >= 3 &&
            isset($data['email']) && strlen($data['email']) < 255 && strpos($data['email'], '@') !== false) {
            
                $newData['ime'] = htmlspecialchars($data['ime'], ENT_QUOTES, 'UTF-8');
                $newData['priimek'] = htmlspecialchars($data['priimek'], ENT_QUOTES, 'UTF-8');
                $newData['email'] = htmlspecialchars($data['email'], ENT_QUOTES, 'UTF-8');
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckProdajalecPassword($data){
        
        if (isset($data['staro_geslo']) && strlen($data['staro_geslo']) < 255 && strlen($data['staro_geslo']) > 5  &&
            isset($data['novo_geslo']) && strlen($data['novo_geslo']) < 255 && strlen($data['novo_geslo']) > 5  &&
            isset($data['novo_geslo_repeat']) && strcmp($data['novo_geslo_repeat'], $data['novo_geslo']) == 0) {
            
                $newData['staro_geslo'] = htmlspecialchars($data['staro_geslo'], ENT_QUOTES, 'UTF-8');
                $newData['novo_geslo'] = htmlspecialchars($data['novo_geslo'], ENT_QUOTES, 'UTF-8');
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckArtikelFields($data){   
        if (isset($data['naziv']) && strlen($data['naziv']) < 255 && strlen($data['naziv']) >= 3 &&
            isset($data['firma']) && strlen($data['firma']) < 255 && strlen($data['firma']) >= 3 &&
            isset($data['cena']) && 
            isset($data['opis']) && strlen($data['opis']) < 1024 &&
            isset($data['kategorija']) && strlen($data['kategorija']) < 255 && strlen($data['kategorija']) >= 3 &&
            isset($data['statusArtikla'])) {
            
                $newData['naziv'] = htmlspecialchars($data['naziv'], ENT_QUOTES, 'UTF-8');
                $newData['firma'] = htmlspecialchars($data['firma'], ENT_QUOTES, 'UTF-8');
                $newData['cena'] = htmlspecialchars($data['cena'], ENT_QUOTES, 'UTF-8');
                $newData['opis'] = htmlspecialchars($data['opis'], ENT_QUOTES, 'UTF-8');
                $newData['kategorija'] = htmlspecialchars($data['kategorija'], ENT_QUOTES, 'UTF-8');
                $newData['statusArtikla'] = htmlspecialchars($data['statusArtikla'], ENT_QUOTES, 'UTF-8');
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckSlikaFields($data){
        if(isset($data["Artikel_id"]) && isset($data["slika"])){
            return true;
        }
        return false;
    }
    
    public static function CheckOcenaFields($data){   
        if (isset($data['Artikel_id']) && isset($data['Stranka_id']) && isset($data['ocena']) && isset($data['opis'])) {
            
                $newData['Artikel_id'] = htmlspecialchars($data['Artikel_id'], ENT_QUOTES, 'UTF-8');
                $newData['Stranka_id'] = htmlspecialchars($data['Stranka_id'], ENT_QUOTES, 'UTF-8');
                $newData['ocena'] = htmlspecialchars($data['ocena'], ENT_QUOTES, 'UTF-8');
                $newData['opis'] = htmlspecialchars($data['opis'], ENT_QUOTES, 'UTF-8');
                
                return $newData;
        }
        
        return NULL;
    }
    
    public static function CheckNarociloFields($data){
        if (isset($data["datum_oddano"]) && isset($data["skupnaCena"]) && isset($data["Stranka_id"]) && isset($data["izdelki"])){
            $newData['datum_oddano'] = htmlspecialchars($data['datum_oddano'], ENT_QUOTES, 'UTF-8');
            $newData['skupnaCena'] = htmlspecialchars($data['skupnaCena'], ENT_QUOTES, 'UTF-8');
            $newData['Stranka_id'] = htmlspecialchars($data['Stranka_id'], ENT_QUOTES, 'UTF-8');
            $newData['izdelki'] = array();
            
            foreach ($data["izdelki"] as $key => $value) {
                if(isset($value["Artikel_id"]) && isset($value["quantity"])){
                    $izdelek['Artikel_id'] = htmlspecialchars($value['Artikel_id'], ENT_QUOTES, 'UTF-8');
                    $izdelek['quantity'] = htmlspecialchars($value['quantity'], ENT_QUOTES, 'UTF-8');
                    
                    $newData["izdelki"][$key] = array("Artikel_id" => $izdelek["Artikel_id"], "quantity" => $izdelek["quantity"]);
                }else{
                    return NULL;
                }
            }
            return $newData;
        }
        return NULL;
    }
    
    public static function CheckProdajalecInput($data) {
        if(isset($data["status"]) && isset($data["datum_odobreno"]) && isset($data["Prodajalec_id"])) {
            $newData['status'] = htmlspecialchars($data['status'], ENT_QUOTES, 'UTF-8');
            $newData['datum_odobreno'] = htmlspecialchars($data['datum_odobreno'], ENT_QUOTES, 'UTF-8');
            $newData['Prodajalec_id'] = htmlspecialchars($data['Prodajalec_id'], ENT_QUOTES, 'UTF-8');
            
            return $newData;
        }
        
        return NULL;
    }
}
