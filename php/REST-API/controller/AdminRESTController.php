<?php

require_once("model/AdminDB.php");
require_once("ViewHelper.php");

class AdminRESTController {

    public static function get($id) {
        try {
            echo ViewHelper::renderJSON(AdminDB::get(["id" => $id[1]]));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No such Admin with given ID");
            echo ViewHelper::renderJSON($data, 404);
        }
    }

    public static function edit($data) {
        try {
            $admin = AdminDB::get(["id" => $data["id"]]);

            if(strcmp($admin["geslo"], hash("sha512", $data["geslo"])) == 0){
                $data["geslo"] = hash("sha512",$data["geslo_novo"]);
                AdminDB::update($data);
                
                $data = array("message" => "Successfull.");
                echo ViewHelper::renderJSON($data, 200); 
            }else{
                throw new Exception();
            }
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Missing data.");
            echo ViewHelper::renderJSON($data, 400); 
        }
    }
    
        public static function getByEmailAndVerify($login) {
            try {
                $admin = AdminDB::getByEmail($login);
                $login['geslo'] = htmlspecialchars($login['geslo'], ENT_QUOTES, 'UTF-8');
                $login['geslo'] = hash('sha512', $login['geslo']);
                $login['email'] = htmlspecialchars($login['email'], ENT_QUOTES, 'UTF-8');

                if($admin['geslo'] === $login['geslo'] && $admin['email'] === $login['email']){
                    $data = array("message"=>"Successfull.", "id" => $admin["id"]);
                    echo ViewHelper::renderJSON($data);
                }else{
                    throw new InvalidArgumentException();
                }
            } catch (InvalidArgumentException $e) {
                $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid login data.");
                echo ViewHelper::renderJSON($data, 404);
            }
    }
}
