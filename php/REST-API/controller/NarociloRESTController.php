<?php

require_once("model/NarociloDB.php");
require_once("ViewHelper.php");

class NarociloRESTController {

    public static function get($id) {
        try {
            echo ViewHelper::renderJSON(NarociloDB::get(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No such Narocilo with given ID");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function add($data) {
        try {
            //vrne id novonastalega narocila
            return NarociloDB::insert($data);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Error inserting artikel.");
            echo ViewHelper::renderJSON($data, 400);
        }     
    }
    
    public static function insertArtikel_has_Narocilo($data){
        try {
            NarociloDB::insertArtikel_has_Narocilo($data);
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function getAll() {
        try {
            echo ViewHelper::renderJSON(NarociloDB::getAll());
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function edit($data) {
        try {
            NarociloDB::update($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Missing data.");
            echo ViewHelper::renderJSON($data, 400); 
        }
    }
    
    public static function getAllForStranka($id) {
        try {
            echo ViewHelper::renderJSON(NarociloDB::getAllForStranka(array("id" => $id)), 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Missing data.");
            echo ViewHelper::renderJSON($data, 400); 
        }
    } 
}