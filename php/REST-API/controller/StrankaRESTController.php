<?php

require_once("model/StrankaDB.php");
require_once("ViewHelper.php");

class StrankaRESTController {

    public static function get($id) {
        try {
            echo ViewHelper::renderJSON(StrankaDB::get(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No such Stranka with given ID");
            echo ViewHelper::renderJSON($data, 404);
        }
    }

    public static function add($data) {
        try {
            $data['geslo'] = hash('sha512', $data['geslo']);
            $id = StrankaDB::insert($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 201);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Email already in use.");
            echo ViewHelper::renderJSON($data, 400);
        }     
    }

    public static function edit($data) {
        try {
            $data['geslo'] = hash('sha512', $data['geslo']);
            StrankaDB::update($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Missing data.");
            echo ViewHelper::renderJSON($data, 400); 
        }
    }

    public static function delete($id) {
        try {
            StrankaDB::delete(["id" => $id]);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No Stranka with given id.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
    
    public static function getAll(){
        try {
            $data = StrankaDB::getAll();
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"There was an error processing the request.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
    
    public static function getByEmailAndVerify($login) {
        try {
            $stranka = StrankaDB::getByEmail($login);
            $login['geslo'] = htmlspecialchars($login['geslo'], ENT_QUOTES, 'UTF-8');
            $login['geslo'] = hash('sha512', $login['geslo']);
            $login['email'] = htmlspecialchars($login['email'], ENT_QUOTES, 'UTF-8');
            
            if($stranka['geslo'] === $login['geslo'] && $stranka['email'] === $login['email']){
                $data = array("message"=>"Successfull.", "id" => $stranka["id"]);
                echo ViewHelper::renderJSON($data);
            }else{
                throw new InvalidArgumentException();
            }
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid login data.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
    
    public static function updateAccountInformation($data){
        try{
            StrankaDB::updateAccountInformation($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Invalid data.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
    
    public static function updatePassword($login){
        try{
            $stranka = StrankaDB::get(["id" => $login["id"]]);
            
            $login['staro_geslo'] = htmlspecialchars($login['staro_geslo'], ENT_QUOTES, 'UTF-8');
            $login['staro_geslo'] = hash('sha512', $login['staro_geslo']);
            
            if($stranka['geslo'] === $login['staro_geslo']){
                $login['novo_geslo'] = hash('sha512', $login['novo_geslo']);
                StrankaDB::updatePassword(["geslo" => $login["novo_geslo"], "id" => $login["id"]]);
                $data = array("message"=>"Successfull.");
                echo ViewHelper::renderJSON($data);
            }else{
                throw new InvalidArgumentException();
            }
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Wrong password.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
}