<?php

require_once("model/ArtikelDB.php");
require_once("ViewHelper.php");

class ArtikelRESTController {

    public static function get($id) {
        try {
            echo ViewHelper::renderJSON(ArtikelDB::get(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No such Artikel with given ID");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function add($data) {
        try {
            ArtikelDB::insert($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 201);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Error inserting artikel.");
            echo ViewHelper::renderJSON($data, 400);
        }     
    }
    
    public static function edit($data) {
        try {
            ArtikelDB::update($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Missing data.");
            echo ViewHelper::renderJSON($data, 400); 
        }
    }
    
    public static function delete($id) {
        try {
            ArtikelDB::delete(["id" => $id]);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 200);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"No Stranka with given id.");
            echo ViewHelper::renderJSON($data, 400);
        }
    }
    
    public static function getAll() {
        try {
            echo ViewHelper::renderJSON(ArtikelDB::getAll());
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
    
    public static function getAllActive($data) {
        try {
            echo ViewHelper::renderJSON(ArtikelDB::getAllActive($data));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
}