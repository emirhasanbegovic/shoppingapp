<?php

require_once("model/OcenaDB.php");
require_once("ViewHelper.php");

class OcenaRESTController {
    
    public static function add($data) {
        try {
            OcenaDB::insert($data);
            $data = array("message"=>"Successfull.");
            echo ViewHelper::renderJSON($data, 201);
        } catch (Exception $ex) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Error inserting artikel.");
            echo ViewHelper::renderJSON($data, 400);
        }     
    }
    
    public static function getAllOcenaByArtikelID($data) {
        try {
            echo ViewHelper::renderJSON(OcenaDB::getAllOceneByArtikelID($data));
        } catch (InvalidArgumentException $e) {
            $data = array("error" => "There was an error", "status"=>400, "message"=>"Empty");
            echo ViewHelper::renderJSON($data, 404);
        }
    }
}